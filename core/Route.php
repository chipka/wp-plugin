<?php

namespace Jpoll;

use App\Controllers;


class Route
{
    private $config;
    private $routes = [];

    public function __construct()
    {
        $this->config = (object)require_once (__DIR__ . "/../config/route.php");
    }

    public function run()
    {
        foreach ($this->routes as $route) {
            register_rest_route($this->config->api, '/' . $route['route'], array(
                'methods'  => $route['method'],
                'callback' => array(
                    new $route['action'][0],
                    $route['action'][1]
                )
            ));
        }
    }

    public function get($route, $action)
    {
        $action = $this->parse($action);
        $action[0] = "App\\Controllers\\" . $action[0];
        
        $this->routes[] = ['route' => $route, 'action' => $action, 'method' => 'GET'];
    }

    public function post($route, $action)
    {
        $action = $this->parse($action);
        $action[0] = "App\\Controllers\\" . $action[0];

        $this->routes[] = ['route' => $route, 'action' => $action, 'method' => 'POST'];
    }

    public function put($route, $action)
    {
        $action = $this->parse($action);
        $action[0] = "App\\Controllers\\" . $action[0];

        $this->routes[] = ['route' => $route, 'action' => $action, 'method' => 'PUT'];
    }

    public function patch($route, $action)
    {
        $action = $this->parse($action);
        $action[0] = "App\\Controllers\\" . $action[0];

        $this->routes[] = ['route' => $route, 'action' => $action, 'method' => 'PATCH'];
    }

    public function delete($route, $action)
    {
        $action = $this->parse($action);
        $action[0] = "App\\Controllers\\" . $action[0];

        $this->routes[] = ['route' => $route, 'action' => $action, 'method' => 'DELETE'];
    }


    public function parse($action)
    {
       return explode('@', $action);
    }

}