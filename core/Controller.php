<?php

namespace Jpoll;

class Controller
{
    protected $db;

    public function __construct()
    {
        // construct
    }

    protected function render($tmp_name, $data = '')
    {
        require_once(__DIR__ . "/../Views/$tmp_name.html");
    }
    
    protected function setTable($table, $columns)
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS $table ($columns)");
    }
}