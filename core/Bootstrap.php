<?php

use App\Controllers;

use Jpoll\Route;
    
$app = new Controllers\ApplicationController($wpdb);
    
$route = new Route;

require_once __DIR__ . "/../routes/route.php";

$cfg = (object)require_once __DIR__ . "/../config/app.php";

   