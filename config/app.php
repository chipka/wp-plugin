<?php
    return [
        'initial' => 'init', // The name of the method in the ApplicationController that will start when the plugin is activated
        'running' => 'run',  // The name of the method in the ApplicationController, which will be run when the short code is activated
        'routes' => 'run', // The name of the method in the core/Route that will be run for registering routes
    ];