<?php
    
    register_activation_hook(dirname(dirname(__FILE__)) .  '\index.php', array( $app, $cfg->initial ));

    add_shortcode( 'wp-plugin', array( $app, $cfg->running ));

    add_action( 'rest_api_init', array( $route, $cfg->routes ));