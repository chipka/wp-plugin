<?php

namespace App\Controllers;

use Jpoll\Controller;

class ApplicationController extends Controller
{
    public function __construct($db)
    {
        $this->db = $db;
    }

    public function run()
    {
        echo "Hello World";
    }
    public function init()
    {
        $this->setTablePoll();

        $this->setTablePollVote();

        $this->setTablePollIp();
    }

    private function setTablePoll()
    {
        $table = $this->db->prefix . "j_poll";

        $columns = "id mediumint(9) NOT NULL AUTO_INCREMENT," .
                   "question text NOT NULL," .
                   "UNIQUE KEY id (id)";

        $this->setTable($table, $columns);
    }

    private function setTablePollVote()
    {
        $table = $this->db->prefix . "j_poll_vote";

        $columns = "id mediumint(9) NOT NULL AUTO_INCREMENT," .
                   "answer varchar(255) NOT NULL," .
                   "votes int(10) DEFAULT '0' NOT NULL," .
                   "UNIQUE KEY id (id)";

        $this->setTable($table, $columns);
    }

    private function setTablePollIp()
    {
        $table = $this->db->prefix . "j_poll_ip";

        $columns = "id mediumint(9) NOT NULL AUTO_INCREMENT," .
                   "answer_id mediumint(9) NOT NULL," .
                   "ip varchar(100)NOT NULL UNIQUE KEY," .
                   "UNIQUE KEY id (id)";

        $this->setTable($table, $columns);
    }
    
}